#
# Build Go Environment
#
FROM ubuntu:20.04 as environment

ENV DEBIAN_FRONTEND noninteractive
ENV INITRD No
ENV LANG en_US.UTF-8
ENV GOVERSION 1.14.4
ENV GOROOT /usr/local/go
ENV GOPATH /go
ENV GOBIN ${GOPATH}/bin
ENV PATH "${GOBIN}:${PATH}"

RUN apt update && \
    apt install -y --no-install-recommends \
    ca-certificates \
    wget \
    git \
    build-essential \
    pkg-config \
    curl \
    unzip

WORKDIR /tmp
RUN wget "https://dl.google.com/go/go${GOVERSION}.linux-amd64.tar.gz" && \
    tar -C /usr/local -xzf go${GOVERSION}.linux-amd64.tar.gz && \
    rm go${GOVERSION}.linux-amd64.tar.gz && \
    ln -s /usr/local/go/bin/go /usr/bin/ && \
    mkdir -p $GOPATH/src/app

RUN go get -u github.com/t-yuki/gocover-cobertura
